extends Node2D


export (String,FILE,"*.tscn") var next_scene = "res://scenes/main_title.tscn"

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	pass 

func next():
	get_tree().change_scene(next_scene)

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().change_scene("res://scenes/main_title.tscn")
