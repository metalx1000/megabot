extends KinematicBody2D

var death = preload("res://objects/explosion/explosion.tscn")
var velocity = Vector2.ZERO
export (int) var speed = 1000
export (int) var jump_speed = 150
var gravity = 500
var dead = false
func _ready():
	pass
	
func _physics_process(delta):
	if dead:
		return
	get_input()
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	
func get_input():
	var delta = get_physics_process_delta_time()
	var x = Input.get_action_strength("player_right") - Input.get_action_strength("player_left")
	var y = Input.get_action_strength("player_down") - Input.get_action_strength("player_up")
	if Input.is_action_just_pressed("ui_accept"):
		death()

	velocity.x = speed * x * delta * 10 
	#velocity.y = 0

	if is_on_floor():
		if x > 0:
			$Sprite.flip_h = false
			$AnimationPlayer.play("run")
		elif x < 0 && is_on_floor():
			$Sprite.flip_h = true
			$AnimationPlayer.play("run")
		else:
			$AnimationPlayer.play("idle")
		if Input.is_action_just_pressed("player_up") :
			velocity.y = jump_speed * -1 * delta * 100
	else:
		$AnimationPlayer.play("jump")

func death():
	dead = true
	visible = false
	var explosion = death.instance()
	explosion.global_position = global_position
	owner.add_child(explosion)
	
