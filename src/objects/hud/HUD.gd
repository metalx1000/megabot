extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



func update():
	if Global.score_current < Global.score:
		$score.play()
		Global.score_current += 10
		$Label.text = "Score: " + str(Global.score_current)
