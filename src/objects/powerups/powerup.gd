extends Area2D

var points = preload("res://objects/points/points.tscn")

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_powerup_body_entered(body):
	if !body.is_in_group("players"):
		return
	
	var point = points.instance()
	point.global_position = global_position
	get_tree().current_scene.add_child(point)
	Global.score += 100
	body.jump_speed += 10
	queue_free()
